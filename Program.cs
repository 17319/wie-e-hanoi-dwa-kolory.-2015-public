﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
namespace Hanoi
{
	class Program
	{
		static public int ilosc_par_krazkow;
		static public bool czy_pokazywac_wyniki;
		static public bool czy_do_pliku;
		static public char rek_iter;
		public static void Main(string[] args)
		{
			pocz:
			try {
				pocz2:
				Console.Write("Algorytm iteracyjny (i) czy rekurencyjny (r): ");
				rek_iter = char.Parse(Console.ReadLine());
				if (rek_iter != 'i' && rek_iter != 'r')
					goto pocz2;
				Console.Write("Podaj liczbę par krążków: ");
				ilosc_par_krazkow = int.Parse(Console.ReadLine());
				pocz3:
				Console.Write("Czy wypisać wyniki na ekran (true / false): ");
				czy_pokazywac_wyniki = bool.Parse(Console.ReadLine());
				if (czy_pokazywac_wyniki != true && czy_pokazywac_wyniki != false)
					goto pocz3;
				pocz4:
				Console.Write("Czy wyniki do pliku (true false): ");
				czy_do_pliku = bool.Parse(Console.ReadLine());
				if (czy_do_pliku != true && czy_do_pliku != false)
					goto pocz4;
				Console.WriteLine("OK");
			} catch {
				Console.WriteLine("źle wpisales, popraw się!");
				goto pocz;
			}
			
			if (rek_iter == 'i')
				hanoi_iter(ilosc_par_krazkow, czy_pokazywac_wyniki, czy_do_pliku);
			if (rek_iter == 'r')
				two_hanoi(ilosc_par_krazkow, czy_pokazywac_wyniki, czy_do_pliku);
			pocz6:
			try {
				bool czy;
				Console.WriteLine("Czy chcesz działać od nowa? (true false)");
				czy = bool.Parse(Console.ReadLine());
				if (czy) {
					Console.Clear();
					goto pocz;
				}
			} catch {
				Console.WriteLine("Popraw się");
				goto pocz6;
			}
		}
		
		static void hanoi_iter(int iloscKrazkow, bool czy_pokazywac_wyniki, bool czy_do_pliku)
		{
			ulong ile = 0;
			File.Delete("wyniki_hanoi_iter.txt");
			Console.WriteLine("Krążków: " + ilosc_par_krazkow * 2);
			Console.WriteLine("Wież: 3");
			if (czy_do_pliku) {
				using (var writer = new StreamWriter("wyniki_hanoi_iter.txt", true)) {
			
					writer.WriteLine(ilosc_par_krazkow * 2);
					writer.WriteLine("3");
				}
			}
			Towers t = new Towers((int)iloscKrazkow);
			if (czy_pokazywac_wyniki) {
				Console.WriteLine("Startowy układ wież:");
				t.Wypisz();
				Pause();
			}
			while (!t.TowersOK()) {
				int min = 0;
				for (int i = 0; i < 3; ++i)
					if (t.towers[i].Count > 0 && t.towers[i].Last().wielkosc == 1) {
						min = i;
						break;
					}
				int nastepny = 0;
				if (t.towers[min].Count % 2 != 1)
					nastepny = min + 1;
				else
					nastepny = min - 1;
				if (nastepny < 0)
					nastepny = 2;
				else if (nastepny > 2)
					nastepny = 0;
				t.PrzeniesElement(min, nastepny);
				t.Wypisz();
				ile++;
				if (t.TowersOK())
					continue;
				int v1 = 0, v2 = 0;
				if (nastepny == 0) {
					v1 = 1;
					v2 = 2;
				} else if (nastepny == 1) {
					v1 = 0;
					v2 = 2;
				} else {
					v1 = 0;
					v2 = 1;
				}
				if (!t.PrzeniesElement(v1, v2))
					t.PrzeniesElement(v2, v1);
				t.Wypisz();
				ile++;
			}
			if (ilosc_par_krazkow == 2 || (ilosc_par_krazkow > 2 && ilosc_par_krazkow % 2 != 0)) {
				t.PrzeniesElement(2, 1);
				t.Wypisz();
				ile++;
			}
			Console.WriteLine("Liczba ruchów: " + ile);
		}
		
		static void hanoi(int ilosc_krazkow, int A, int B, int C, ref ulong krok_rek, ref Towers t)
		{
			if (ilosc_krazkow > 0) {
				int zmn;
				if (ilosc_krazkow % 2 == 0)
					zmn = 2;
				else
					zmn = 1;
				hanoi(ilosc_krazkow - zmn, A, C, B, ref krok_rek, ref t);
				if (czy_do_pliku)
					using (var writer = new StreamWriter("wyniki_hanoi_rek.txt", true)) {
						writer.WriteLine(A + " " + C);
						if (zmn == 2)
							writer.WriteLine(A + " " + C);
					}
				t.PrzeniesElement(A - 1, C - 1);
				krok_rek++;
				if (czy_pokazywac_wyniki)
					t.Wypisz();
				if (zmn == 2) {
					t.PrzeniesElement(A - 1, C - 1);
					krok_rek++;
					if (czy_pokazywac_wyniki)
						t.Wypisz();
				}
				hanoi(ilosc_krazkow - zmn, B, A, C, ref krok_rek, ref t);
			}
		}

		static void two_hanoi(int ilosc_par_krazkow, bool czy_pokazywac_wyniki, bool czy_do_pliku)
		{
			ulong krok_rek = 0;
			int ilosc_krazkow = ilosc_par_krazkow * 2;
			Towers t = new Towers((int)ilosc_par_krazkow);
			if (czy_pokazywac_wyniki) {
				Console.WriteLine("Startowy układ wież:");
				t.Wypisz();
				Pause();
			}
			if (czy_pokazywac_wyniki) {
				Console.WriteLine("Krążków: " + ilosc_krazkow);
				Console.WriteLine("Wież: 3");
			}
			if (czy_do_pliku) {
				File.Delete("wyniki_hanoi_rek.txt");
				using (var writer = new StreamWriter("wyniki_hanoi_rek.txt", true)) {
					writer.WriteLine(ilosc_krazkow);
					writer.WriteLine("3");
				}
			}
			int zamiennik, a = 1, b = 2, e = 1;
			ilosc_krazkow -= 2;
			while (ilosc_krazkow >= 0) {
				hanoi(ilosc_krazkow, a, 3, b, ref krok_rek, ref t);
				krok_rek++;
				if (czy_do_pliku)
					using (var writer = new StreamWriter("wyniki_hanoi_rek.txt", true)) {
						writer.WriteLine(a + " " + 3);
					}
				t.PrzeniesElement(a - 1, 2);
				if (czy_pokazywac_wyniki)
					t.Wypisz();
				ilosc_krazkow -= e;
				e = (e + 2) % 4;
				zamiennik = a;
				a = b;
				b = zamiennik;
			}
			if (czy_pokazywac_wyniki) {
				Console.WriteLine("Zadanie wykonano w " + krok_rek + " krokach dla " + ilosc_par_krazkow * 2 + " krążków.");
			}
		}
		
		public static void Pause()
		{
			Console.Write("Naciśnij dowolny klawisz aby kontynuować");
			Console.ReadKey(true);
		}
	}
	
	class Element
	{
		public string kolor;
		public int wielkosc;
		public Element(string kolor_, int wielkosc_)
		{
			kolor = kolor_;
			wielkosc = wielkosc_;
		}
	}
	
	class Towers
	{
		public List <List<Element>> towers;
		public Towers(int ilosc)
		{
			towers = new List<List<Element>>();
			for (int i = 0; i < 3; ++i)
				towers.Add(new List<Element>());
			for (int i = 0; i < ilosc; ++i) {
				towers[0].Add(new Element("B", ilosc - i));
				towers[0].Add(new Element("C", ilosc - i));
			}
		}
		public bool PrzeniesElement(int od, int do_)
		{
			int od_2, do_2;
			if (do_ >= towers.Count || do_ < 0 || towers[od].Count == 0)
				return false;
			Element e = towers[od].Last();
			if (towers[do_].Count > 0 && e.wielkosc > towers[do_].Last().wielkosc)
				return false;
			towers[od].RemoveAt(towers[od].Count - 1);
			towers[do_].Add(e);
			od_2 = od + 1;
			do_2 = do_ + 1;
			
			if (Program.czy_pokazywac_wyniki)
				Console.WriteLine("Zamiana " + od_2 + " " + do_2);
			if (Program.czy_do_pliku) {
				using (var writer = new StreamWriter("wyniki_hanoi_iter.txt", true)) {
			
					writer.WriteLine(od_2 + " " + do_2);
				}
			}
			return true;
		}
		public void Wypisz()
		{
			if (Program.czy_pokazywac_wyniki) {
				for (int i = 0; i < 3; ++i) {
					Console.Write("TOWER " + i.ToString() + " ");
					for (int j = 0; j < towers[i].Count; ++j)
						Console.Write("[" + towers[i][j].kolor + " " + towers[i][j].wielkosc + "]");
					Console.Write("\n");
				}
				Console.WriteLine("--------");
			}
		}
		public bool TowersOK()
		{	
			int n = 0;
			for (int i = 0; i < 3; ++i) {
				for (int j = 0; j < towers[i].Count - 1; ++j) {
					if (towers[i][j].kolor != towers[i][j + 1].kolor || towers[i][j].wielkosc < towers[i][j + 1].wielkosc)
						return false;
				}
				if (towers[i].Count == 0)
					++n;
			}
			if (n != 1 && (Program.ilosc_par_krazkow > 2 && Program.ilosc_par_krazkow % 2 == 0))
				return false;
			return true;
		}
	}
}